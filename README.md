# Penpot Components for GNOME

![preview image](https://gitlab.gnome.org/michaelkronenberg/penpot-components/-/wikis/uploads/eb06ffa2620f25685a0975b38b492666/gnome-penpot-preview.png)

## What is penpot?

[penpot.app](https://penpot.app/) is a free open source alternative to Figma/Sketch/Adobe XD. In additional to their free cloud hosted app, they offer the ability to self-host. They have indicated the goal is to remain free and open source, with an eventual paid enterprise tier.

The goals of penpot align with the open source community, which is why it has seen adoption from the Fedora team as well as Blender and Mozilla.

## Benefits

The goal of this component library is to offer designers interested in GNOME with a familiar workflow when coming from the broader software world.

#### Feature Highlights

* Saved components with the ability to override
* Nested components
* Organized color palettes
* Organized typography styles
* CSS flexbox layouts

## Intention of this Project

The intention of creating this project is to exlore the viability of penpot as a modern alternative to Inkscape.

#### Challenges

penpot's SVG export renders correctly in the web browser but does not render correctly in the GNOME Image Viewer or Inkscape. If you are knowledgeable with Inkscape, I'm interested in identifying penpot to inkscape best practices.

## Getting Started with penpot

* Sign up for a free online account at [penpot.app](https://penpot.app/) - Easiest Option
* [Self-host](https://penpot.app/self-host) penpot
* [Run locally](https://blog.linuxgrrl.com/2022/01/19/running-penpot-locally-docker-free-with-podman/) using Podman

## Using this Library

1. Download [GNOME-Components.penpot](GNOME-Components.penpot) and upload it to your penpot account
2. Upload the correct version of Cantarell font to your penpot account. [Instructions in the wiki](https://gitlab.gnome.org/michaelkronenberg/penpot-components/-/wikis/Guides/Installing-Cantarell)
3. Add GNOME-components as a [shared library](https://help.penpot.app/user-guide/libraries/#shared-libraries) in your penpot account
