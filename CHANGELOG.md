# February 21, 2023

##### Updated

* Components and Colors now align to naming conventions in libadwaita docs
* Components now reference the HIG, libadwaita Docs and Gtk Docs
* Use of Flex layouts introduced in penpot 1.17

##### Added

* Avatar
* Banners
* List Row Types: Action, Expander, Entry, Password Entry 
* Buttons: class types .ButtonContent & .osd
* Carousels
* Entry
* Preferences
* Progress Bar
* Sliders
* Tabs
* Toast Overlay
* Toolbars
* Spin Button

# January 28, 2023

##### Added

* Checkboxes
* Radio 
* Switches
